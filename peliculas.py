def noRepetidas(peliculas):
    
    for i in peliculas:
        if(peliculas.count(i)>1):
            return False
    return True

def gustanAUno(peliculaUno,peliculaDos,peliculaTres):

    if(noRepetidas(peliculaUno) and noRepetidas(peliculaDos) and noRepetidas(peliculaTres)):
        peliculas = peliculaUno+peliculaDos+peliculaTres
        uno = []
        for i in peliculas:
            if(peliculas.count(i)== 1):
                uno.append(i)
        return uno
    
    return False

def gustanADos(peliculaUno,peliculaDos,peliculaTres):
    
    if(noRepetidas(peliculaUno) and noRepetidas(peliculaDos) and noRepetidas(peliculaTres)):
        peliculas = peliculaUno+peliculaDos+peliculaTres
        comunes = []
        for i in peliculas:
            if(peliculas.count(i) == 2 and not(i in comunes)):
                comunes.append(i) 
        return comunes
    
    return False

def gustanATres(peliculaUno,peliculaDos,peliculaTres):
    
    if(noRepetidas(peliculaUno) and noRepetidas(peliculaDos) and noRepetidas(peliculaTres)):
        peliculas = peliculaUno+peliculaDos+peliculaTres
        aTodos = []
        for i in peliculas:
            if(peliculas.count(i) == 3 and not(i in aTodos)):
                aTodos.append(i) 
        return aTodos
    
    return False


pelicula1 = ["Titanic","Super sonic","The mechanic 1","The invisible"]
pelicula2 = ["Titanic","Rambo","The mechanic 2"]
pelicula3 = ["Titanic","Super sonic","Jumanji","The invisible"]


############# test #############

def test_gustanAuno():
    aUno = gustanAUno(pelicula1, pelicula2, pelicula3)
    esperado = ["The mechanic 1","Rambo","The mechanic 2","Jumanji"]
    assert aUno == esperado

def test_gustanAdos():
    aDos = gustanADos(pelicula1, pelicula2, pelicula3)
    esperado = ["Super sonic","The invisible"]
    assert aDos == esperado

def test_gustanAtres():
    aTres = gustanATres(pelicula1, pelicula2, pelicula3)
    esperado = ["Titanic"]
    assert aTres == esperado

def test_nogustanAuno():
    aUno = gustanAUno(pelicula1, pelicula2, pelicula3)
    esperado = ["The mechanic 1","Rambo","Jumanji"]
    assert aUno != esperado

def test_nogustanAdos():
    aDos = gustanADos(pelicula1, pelicula2, pelicula3)
    esperado = ["The invisible"]
    assert aDos != esperado

def test_nogustanAtres():
    aTres = gustanATres(pelicula1, pelicula2, pelicula3)
    esperado = ["The invisible"]
    assert aTres != esperado

    
################################

