def calcularAnagramas(vector):
    
    palabra = vector.pop(0)
    return [i for i in vector if palabra!=i and (sorted(palabra)==sorted(i))]

def anagramas(vector):

    if(len(vector)>1):
        numeroAnagramas = len(calcularAnagramas(vector))
        return numeroAnagramas
    else:
        return False



############## Test ##############

def test_anagramas():
    numeroanagramas = anagramas(["abc","bac","acb","abc"])
    esperado  = 2
    assert numeroanagramas == esperado

def test_noanagramas():
    numeroanagramas = anagramas(["abc","zzz","ww","abd"])
    esperado  = 0
    assert numeroanagramas == esperado

def test_anagramastodosiguales():
    numeroanagramas = anagramas(["abc","abc","abc","abc","abc"])
    esperado  = 0
    assert numeroanagramas == esperado


###################################