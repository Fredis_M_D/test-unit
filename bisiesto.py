def esbisiesto(anio):
    return ((anio % 4 == 0 and anio % 100 != 0) or anio % 400 == 0)

############## Test ##############

def test_divisiblePorCuatro():
    anio = esbisiesto(1995)
    esperado = False
    assert anio == esperado

def test_esbisiesto():
    anio = esbisiesto(2000)
    bisiesto = True
    assert anio == bisiesto 

def test_nobisiesto():
    anio = esbisiesto(2100)
    bisiesto = False
    assert anio == bisiesto 

###################################