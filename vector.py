def generarVector(size):    
    return [i for i in range(size)]

def ordenarPar(vector):
    par = []
    for i in vector:
        if(i % 2 == 0):
            par.append(i)
    par = sorted(par, reverse = True)
    par.insert(0,par.pop(-1))
    return par

def ordenarImpar(vector):
    impar = []
    for i in vector:
        if(i % 2 != 0):
            impar.append(i)
    return sorted(impar)

def imparCeroPar(size):

    if(size > 1):
       imparceropar = []
       vector = generarVector(size)
       Par = ordenarPar(vector)
       Impar = ordenarImpar(vector) 
       imparceropar = Impar+Par
       return imparceropar
    else:
        return False

    
############# test #############

def test_generarVector():
    vector = generarVector(11)
    esperado = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    assert vector == esperado

def test_sizeOne():
    vector = imparCeroPar(-1)
    esperado = False
    assert vector == esperado

def test_imparceroparImpar():
    vector = imparCeroPar(11)
    esperado = [1, 3, 5, 7, 9, 0, 10, 8, 6, 4, 2]
    assert vector == esperado

def test_imparceroparPar():
    vector = imparCeroPar(10)
    esperado = [1, 3, 5, 7, 9, 0, 8, 6, 4, 2]
    assert vector == esperado

def test_cero():
    vector = imparCeroPar(10)
    esperado = [1, 3, 5, 7, 9, 8, 6, 4, 2,0]
    assert vector != esperado


################################
