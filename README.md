# Taller de test unit #

# Anagramas #
 Una palabra es anagrama de otra si las dos tienen las mismas letras, con el mismo número de apariciones, pero en un orden diferente(Wikipedia).

# Bisiesto #
 Año bisiesto es el divisible entre 4, salvo que sea año secular -último de cada siglo, terminado en «00»-, en cuyo caso también ha de ser divisible entre 400(Wikipedia).

# Peliculas #
 Peliculas vistas por tres personas, se obtiene las comunes entre los personas, las que solo ven dos personas y las que solo le gustan a uno persona.

# Salarios #
 Salarios y nombres de personas organizados en dos vectores, ordenados de acuerdo a la posición. 
 Names(0) es salarios(0). De manera que para persona n, corresponderá a names(n) y salarios(n).
 
# Vector #
 Dado un número mayor que uno generar un vector con los impares ordenados de menor a mayor luego el cero y 
 depués los pares organizados de mayor a menor.  

